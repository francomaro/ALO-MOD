; #FUNCTION# ====================================================================================================================
; Name ..........: ALO MOD  MOD Functions
; Description ...: This file Includes several files in the current script.
; Syntax ........: #include
; Parameters ....: ---
; Return values .: ---
; Author ........: ALO MOD Titi
; Modified ......: 08/05/2017
; RemaALO MOD s .......: This file is part of MyBot, previously known as ClashGameBot. Copyright 2015-2017
;                  MyBot is distributed under the terms of the GNU GPL
; Related .......: ---
; Link ..........: https://github.com/MyBotRun/MyBot/wiki
; Example .......: ---
; ===============================================================================================================================

#include "ALO MOD CSV Speed.au3"
#include "ALO MOD Four Finger Classic.au3"
#include "ALO MOD GoblinXP\ALO MOD GoblinXP.au3"
#include "ALO MOD GoblinXP\ALO MOD multiSearch.au3"
#include "ALO MOD GoblinXP\ALO MOD ArrayFunctions.au3"

#include "ALO MOD Forecast.au3"

#include "ALO MOD AF_SCID\AF_SCID Switch Acc.au3"
#include "ALO MOD AF_SCID\AF_Various ImgSearch.au3"

#include "ALO MOD Auto update camps.au3" ; (#ID135-)
#include "ALO MOD Check Stop For War\CheckStopForWar.au3"

#include "ALO MOD Switch Profiles.au3"

;GTFO - Added by ALO MOD  MOD
#include "ALO MOD GTFO\GTFO.au3"
#include "ALO MOD GTFO\KickOut.au3"

; Bot Humanization - Added by ALO MOD  MOD
#include "ALO MOD Bot Humanization\BotHumanization.au3"
#include "ALO MOD Bot Humanization\AttackNDefenseActions.au3"
#include "ALO MOD Bot Humanization\BestClansNPlayersActions.au3"
#include "ALO MOD Bot Humanization\ChatActions.au3"
#include "ALO MOD Bot Humanization\ClanActions.au3"
#include "ALO MOD Bot Humanization\ClanWarActions.au3"


; getOcr - Added by ALO MOD  MOD
#include "ALO MOD OCR\getMyOcr.au3"


;NEW ChatBot by ALO MOD  MOD
#include "ALO MOD Chatbot\Chatbot.au3"
#include "ALO MOD Chatbot\ChatFunc.au3"
#include "ALO MOD Chatbot\Multy Lang.au3"

; Boost for Magic Spell by ALO MOD  MOD
#include "ALO MOD Boost for Magic Spell\ALO MOD Boost for Magic Spell.au3"
#include "ALO MOD Boost for Magic Spell\ALO MOD BoostAllWithMagicSpell.au3"

; Multi Fingers Vectors - (LunaEclipse) - Added by ALO MOD  MOD
#include "ALO MOD Multi Fingers\Vectors\4FingerStandard.au3"
#include "ALO MOD Multi Fingers\Vectors\4FingerSpiralLeft.au3"
#include "ALO MOD Multi Fingers\Vectors\4FingerSpiralRight.au3"
#include "ALO MOD Multi Fingers\Vectors\8FingerPinWheelLeft.au3"
#include "ALO MOD Multi Fingers\Vectors\8FingerPinWheelRight.au3"
#include "ALO MOD Multi Fingers\Vectors\8FingerBlossom.au3"
#include "ALO MOD Multi Fingers\Vectors\8FingerImplosion.au3"
; Multi Fingers Profile - (LunaEclipse) - Added by ALO MOD  MOD
#include "ALO MOD Multi Fingers\4Fingers.au3"
#include "ALO MOD Multi Fingers\8Fingers.au3"
#include "ALO MOD Multi Fingers\MultiFinger.au3"
#include "ALO MOD Multi Fingers\UnitInfo.au3"

#include "ALO MOD Check Warden Mode.au3"

;Wall/Building Upgrading Priority 
#include "ALO MOD Upgrading Priority.au3"

#include "ALO MOD AreCollectorsOutside.au3"

;Demen's DoubleTrain
#include "ALO MOD DoubleTrain.au3"

;Demen's Smart Train
#include "ALO MOD Smart Train\SmartTrain.au3"
#include "ALO MOD Smart Train\CheckQueue.au3"
#include "ALO MOD Smart Train\CheckTrainingTab.au3"
#include "ALO MOD Smart Train\CheckPreciseArmyCamp.au3"


#include "ALO MOD UpgradesMgmt.au3"

#include "ALO MOD Config.au3"
